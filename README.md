# Ruines

`Ruines` is a french video game that explores the _ruins_ of an old fallen
kingdom (see, we have many humour). The code of the game is in **very** early
phase, but the universe is quite advanced.

## Licensing

The code (ie, everything in the `src` directory) is licensed under
the MIT license, while the art (ie, everything in the `assets` directory) is
licensed under the CC-BY-SA license.

Copyright (c) 2019-2022 Eolien55, Timinou
