{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 15,
  "bbox_right": 84,
  "bbox_top": 20,
  "bbox_bottom": 79,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 100,
  "height": 100,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default"
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {
      "compositeImage": {
        "FrameId": {
          "name": "e4c7f4b8-f913-4b09-b949-ffb0e69dc498",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "e4c7f4b8-f913-4b09-b949-ffb0e69dc498",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "e4c7f4b8-f913-4b09-b949-ffb0e69dc498",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    },
    {
      "compositeImage": {
        "FrameId": {
          "name": "68a67aea-03f4-46d5-bb83-77c767c8f453",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "68a67aea-03f4-46d5-bb83-77c767c8f453",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "68a67aea-03f4-46d5-bb83-77c767c8f453",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    },
    {
      "compositeImage": {
        "FrameId": {
          "name": "b59d1c03-0b18-4f46-b4f0-eb0ca1535d9f",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "b59d1c03-0b18-4f46-b4f0-eb0ca1535d9f",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "b59d1c03-0b18-4f46-b4f0-eb0ca1535d9f",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    },
    {
      "compositeImage": {
        "FrameId": {
          "name": "0705a0fa-89b0-4484-9dad-9eed44c2ad07",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "0705a0fa-89b0-4484-9dad-9eed44c2ad07",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "0705a0fa-89b0-4484-9dad-9eed44c2ad07",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    },
    {
      "compositeImage": {
        "FrameId": {
          "name": "be05c4b3-4143-4d32-b188-9a9cbb7a28d4",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "be05c4b3-4143-4d32-b188-9a9cbb7a28d4",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "be05c4b3-4143-4d32-b188-9a9cbb7a28d4",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    },
    {
      "compositeImage": {
        "FrameId": {
          "name": "d3f5c2cd-baaf-436e-bb96-e7cb6985d3f2",
          "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
        },
        "LayerId": null,
        "resourceVersion": "1.0",
        "name": "composite",
        "tags": [],
        "resourceType": "GMSpriteBitmap"
      },
      "images": [
        {
          "FrameId": {
            "name": "d3f5c2cd-baaf-436e-bb96-e7cb6985d3f2",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "LayerId": {
            "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
            "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
          },
          "resourceVersion": "1.0",
          "name": "",
          "tags": [],
          "resourceType": "GMSpriteBitmap"
        }
      ],
      "parent": {
        "name": "sPlayer_Run",
        "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
      },
      "resourceVersion": "1.0",
      "name": "d3f5c2cd-baaf-436e-bb96-e7cb6985d3f2",
      "tags": [],
      "resourceType": "GMSpriteFrame"
    }
  ],
  "sequence": {
    "spriteId": {
      "name": "sPlayer_Run",
      "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
    },
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 24,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1,
    "length": 6,
    "events": {
      "Keyframes": [],
      "resourceVersion": "1.0",
      "resourceType": "KeyframeStore<MessageEventKeyframe>"
    },
    "moments": {
      "Keyframes": [],
      "resourceVersion": "1.0",
      "resourceType": "KeyframeStore<MomentsEventKeyframe>"
    },
    "tracks": [
      {
        "name": "frames",
        "spriteId": null,
        "keyframes": {
          "Keyframes": [
            {
              "id": "fafac856-c88b-445b-bbe1-bd167feda454",
              "Key": 0,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "e4c7f4b8-f913-4b09-b949-ffb0e69dc498",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            },
            {
              "id": "73551fea-c44f-4d48-8b9c-4f9ef96084d8",
              "Key": 1,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "68a67aea-03f4-46d5-bb83-77c767c8f453",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            },
            {
              "id": "cbc20075-67c7-422f-9098-d94a3935859a",
              "Key": 2,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "b59d1c03-0b18-4f46-b4f0-eb0ca1535d9f",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            },
            {
              "id": "b108cd17-88d4-4862-a763-731aaae40b93",
              "Key": 3,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "0705a0fa-89b0-4484-9dad-9eed44c2ad07",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            },
            {
              "id": "d5f27c42-d4e2-44f8-a45c-341ba8360540",
              "Key": 4,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "be05c4b3-4143-4d32-b188-9a9cbb7a28d4",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            },
            {
              "id": "f0fa2eea-6953-4329-a87b-e2484bd941cf",
              "Key": 5,
              "Length": 1,
              "Stretch": false,
              "Disabled": false,
              "IsCreationKey": false,
              "Channels": {
                "0": {
                  "Id": {
                    "name": "d3f5c2cd-baaf-436e-bb96-e7cb6985d3f2",
                    "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
                  },
                  "resourceVersion": "1.0",
                  "resourceType": "SpriteFrameKeyframe"
                }
              },
              "resourceVersion": "1.0",
              "resourceType": "Keyframe<SpriteFrameKeyframe>"
            }
          ],
          "resourceVersion": "1.0",
          "resourceType": "KeyframeStore<SpriteFrameKeyframe>"
        },
        "trackColour": 0,
        "inheritsTrackColour": true,
        "builtinName": 0,
        "traits": 0,
        "interpolation": 1,
        "tracks": [],
        "events": [],
        "modifiers": [],
        "isCreationTrack": false,
        "resourceVersion": "1.0",
        "tags": [],
        "resourceType": "GMSpriteFramesTrack"
      }
    ],
    "visibleRange": {
      "x": 0,
      "y": 0
    },
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0,
    "backdropYOffset": 0,
    "xorigin": 50,
    "yorigin": 50,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {
      "name": "sPlayer_Run",
      "path": "sprites/sPlayer_Run/sPlayer_Run.yy"
    },
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence"
  },
  "layers": [
    {
      "visible": true,
      "isLocked": false,
      "blendMode": 0,
      "opacity": 100,
      "displayName": "default",
      "resourceVersion": "1.0",
      "name": "c5a05652-2316-4549-a69f-444fde99d9ea",
      "tags": [],
      "resourceType": "GMImageLayer"
    }
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy"
  },
  "resourceVersion": "1.0",
  "name": "sPlayer_Run",
  "tags": [],
  "resourceType": "GMSprite"
}
